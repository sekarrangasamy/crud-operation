var mongoose=require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new mongoose.Schema({
	name:{
		type: String,
		//required: true
	},
	email:{
		type: String,
		//required: true
	},
	password:{
		type: String,
		//required: true
	},
	contact:[{
			homephone:{
				type:Number
			},
			officephone:{
				type:Number
			}
		}],
	address:{
		type:String
	},
	age:{
		type: Number
	},
	marritalstatus:{
		type: Boolean,
		default:false
	},

})
module.exports = mongoose.model('User', UserSchema);