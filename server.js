var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
const path = require('path');
var mongoose = require('mongoose');
var UserSchema = require('./user.model.js');
var app = express();
const router = express.Router();
const message = 'deleted';
mongoose.connect('mongodb://databaseuser:databaseuser1@ds143000.mlab.com:43000/create', function (err) {
   if (err) {
       console.log("ERROR", err)
    } else {
       console.log("SUCCESS")
    }
})

app.set('port', (process.env.PORT || 5000));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use("/index", function (req, res) {
    res.sendFile(__dirname + "/index.html");
});


app.use("/newuser_page", function (req, res) {
    res.sendFile(__dirname + "/newuser.html");
});

app.use("/newuser_page/:id", function (req, res) {
    res.sendFile(__dirname + "/newuser.html");
});

app.use("/listofuser", function (req, res) {
    res.sendFile(__dirname + "/listofuser.html");
});


//save user
app.post("/newuser", function (req, res) {
    console.log("req12345",req.body);
    var myData = new UserSchema({
        name: req.body.name,
        email: req.body.email,
		password: req.body.password,
        contact: [{
            homephone: req.body.homephone,
            officephone: req.body.officephone
        }],
        address: req.body.address,
        age: req.body.age,
        marritalstatus: req.body.marritalstatus
    });
    console.log("myData",myData);
    myData.save()
        .then(data => {
            res.sendFile(__dirname + "/listofuser.html");
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Note."
            });
        });
});

//get user
app.get('/listuser', function (req, res) {
    //console.log("res",res);
    UserSchema.find(function (err, data) {
        if (err) {
            res.send(err);
        } else {
            return res.send(data);
        }
    });
});
//single user get
app.get('/listuser/:id', function (req, res) {
	console.log("req.params",req.params.id);
    UserSchema.findById(req.params.id,function (err, data) {
		console.log("err",err);
        if (err) {
            res.send(err);
        } else {
            return res.send(data);
        }
    });
});


// delete user
app.delete("/listuser/:id", function (req, res) {
    UserSchema.findOneAndDelete({ "_id" : req.params.id} ,function(err,data){
		if(!data){
			res.send(err);
		}else{
			return res.json({message});
		}
	});
});


app.put("/newuser/update/:id", function (req, res) {
	console.log("update req",req.body);
    var myData = new UserSchema({
        name: req.body.name,
        email: req.body.email,
		password: req.body.password,
        contact: [{
            homephone: req.body.homephone,
            officephone: req.body.officephone
        }],
        address: req.body.address,
        age: req.body.age,
        marritalstatus: req.body.marritalstatus
    });
    UserSchema.findByIdAndUpdate(req.params.id,{$set:req.body},function(err,data){
        if(err){
            console.log(err)
        }
        else{
			console.log("data123",data);
            return res.json(data);
        }
    })
});


app.listen(5000,function(){
    console.log("RUNNING")
})


